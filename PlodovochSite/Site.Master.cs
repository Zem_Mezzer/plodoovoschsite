﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite
{
    public partial class SiteMaster : MasterPage
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MenuItem root = new MenuItem();
                root.Text = "Магазины";
                root.Selectable = false;
                

                Data.OleDbWorker worker = new Data.OleDbWorker();
                var shops = worker.Read("Shops");

                for (int i = 0; i < shops.Count; i++)
                {
                    MenuItem shop = new MenuItem();
                    shop.Text = shops[i]["Shop_Name"].ToString();
                    shop.NavigateUrl= "/Pages/Shop.aspx?para="+ shops[i]["ID"].ToString();
                    root.ChildItems.Add(shop);
                }

                menu.Items.Add(root);
            }
        }

    }
}