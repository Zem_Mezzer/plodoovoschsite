﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

using System.Linq;
using System.Web;

namespace PlodovochSite.Data
{
    public class OleDbWorker
    {
        OleDbConnection connection;
        public OleDbWorker()
        {
            string _connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + HttpContext.Current.Server.MapPath("/DataBase") + "/DB.mdb"; 
            connection = new OleDbConnection(_connectionString);
        }

        public List<Dictionary<object,object>> Read(string table_Name, string whereKey="", string whereValue="")
        {
            string query="";

            if (whereKey == "")
            {
                query = "SELECT * FROM " + table_Name + " ORDER BY ID";
            }
            else
            if (whereKey != "")
            {
                query = "SELECT * FROM " + table_Name + " WHERE " + whereKey + " = " + whereValue + " ORDER BY ID";
            }

            List<Dictionary<object,object>> DataBaseValues = new List<Dictionary<object,object>>();

            connection.Open();

            OleDbCommand command = new OleDbCommand(query, connection);

            OleDbDataReader reader = command.ExecuteReader();

            int j = 0;
            while (reader.Read())
            {
                DataBaseValues.Add(new Dictionary<object, object>());
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    DataBaseValues[j].Add(reader.GetName(i), reader.GetValue(i));
                }
                j++;
            }
            connection.Close();
            reader.Close();
            return DataBaseValues;
        }

        public void Execute(string query, Dictionary<string, object> parameters = null)
        {
            connection.Open();

            OleDbCommand command = new OleDbCommand(query, connection);

            if (parameters != null)
            {
                for(int i = 0; i < parameters.Count; i++)
                {
                    command.Parameters.AddWithValue(parameters.ElementAt(i).Key, parameters.ElementAt(i).Value);
                }
            }

            command.ExecuteNonQuery();


            connection.Close();
        }
    }
}