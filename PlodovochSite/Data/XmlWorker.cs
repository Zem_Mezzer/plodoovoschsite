﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace PlodovochSite.Data
{
    public class XmlWorker
    {
        public List<Dictionary<string,string>> Read(string Root, string xmlFileName)
        {
            List<Dictionary<string, string>> xmlText = new List<Dictionary<string, string>>();
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpContext.Current.Server.MapPath("/Data") + "/"+xmlFileName+".xml");

            XmlNodeList root = xml.GetElementsByTagName(Root);

            for(int i = 0; i < root.Count; i++)
            {
                Dictionary<string, string> element = new Dictionary<string, string>();
                
                for (int j = 0; j < root[i].ChildNodes.Count; j++)
                {
                    element.Add(root[i].ChildNodes[j].Name, root[i].ChildNodes[j].InnerText);

                    
                }
                xmlText.Add(element);
            }

            return xmlText;
        }
        
    }
}