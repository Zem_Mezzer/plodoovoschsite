﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite.Forms
{
    public partial class Validate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void CheckValidate()
        {
            
        }

        protected void Validate_Click(object sender, EventArgs e)
        {

            Data.OleDbWorker worker = new Data.OleDbWorker();
            var users = worker.Read("Users");


            for(int i = 0;i<users.Count;i++)
            {
                if (Login.Text == users[i]["User_Login"].ToString() && Password.Text == users[i]["User_Password"].ToString())
                {
                    HttpCookie cookie = new HttpCookie("UserData");
                    cookie["login"] = Login.Text;
                    cookie["password"] = Password.Text;
                    cookie["prefix"] = users[i]["User_Prefix"].ToString();
                    cookie.Expires.AddMinutes(1);
                    Response.Cookies.Add(cookie);
                    Response.Redirect("Admin.aspx");
                }
            }
        }
    }
}