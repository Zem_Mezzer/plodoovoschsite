﻿<%@ Page Title="Вход" Language="C#" AutoEventWireup="true" CodeBehind="Validate.aspx.cs" Inherits="PlodovochSite.Forms.Validate" MasterPageFile="~/Site.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="content">
        <h1><%: Title %></h1>
        <p>
            <asp:TextBox ID="Login" runat="server"></asp:TextBox>
            <asp:Label runat="server">Логин</asp:Label>
        </p>
        <p>
            <asp:TextBox ID="Password" runat="server"></asp:TextBox>
            <asp:Label runat="server">Пароль</asp:Label>
        </p>
        <asp:Button runat="server" Text="Вход" OnClick="Validate_Click"/>
    </div>
</asp:Content>
