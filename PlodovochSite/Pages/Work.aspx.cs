﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite
{
    public partial class Work : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Data.OleDbWorker worker = new Data.OleDbWorker();
            var vacancies = worker.Read("Vacancies");

            for (int i = vacancies.Count - 1; i >= 0; i--)
            {
                Panel block = new Panel
                {
                    CssClass = "blog"
                };

                Label header = new Label();
                header.Font.Size = 16;
                header.Text = "<strong><p><a href = \"Vacancy.aspx?para="+ vacancies[i]["ID"]+ "\" >" + vacancies[i]["Vacancy_Name"] + "</a></p></strong><hr/>";

                Label description = new Label();
                description.Font.Size = 12;
                description.Text = "<p style=\"margin-top:30px; margin-bottom:30px\">" + vacancies[i]["Vacancy_Description"] + "</p>";


                block.Controls.Add(header);
                block.Controls.Add(description);

                blog.Controls.Add(block);
            }
        }
    }
}