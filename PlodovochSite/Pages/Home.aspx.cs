﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace PlodovochSite
{
    public partial class MainPage : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            

            BlogLoad();
        }

        void BlogLoad()
        {


            Data.OleDbWorker worker = new Data.OleDbWorker();
            var blogs = worker.Read("Blog");
            
            for (int i = blogs.Count - 1; i >= 0; i--)
            {
                Panel block = new Panel
                {
                    CssClass = "blog Margin"
                };
                block.ID = "elem";

                Label header = new Label();
                header.Font.Size = 16;
                header.Text = "<strong><p><a href = \"Blog.aspx?para=" + blogs[i]["ID"] + "\">" + blogs[i]["Blog_Name"] + "</a></p></strong>";

                Label description = new Label();
                description.Font.Size = 14;
                byte[] bytes = (byte[])blogs[i]["Blog_Image"];
                description.Text += "<img class=\"image\" src=\"" + "data:Image/png;base64," + Convert.ToBase64String(bytes, 0, bytes.Length) + "\">";
                description.Text += "<p>" + blogs[i]["Blog_Description"] + "</p>";


                
                

                block.Controls.Add(header);
                block.Controls.Add(description);
                blog.Controls.Add(block);
            }
        }

        
    }
}