﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite.Forms
{
    public partial class Shops : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShopsNamesLoad();
        }

        void ShopsNamesLoad()
        {
            Data.OleDbWorker worker = new Data.OleDbWorker();
            var shops = worker.Read("Shops");

            for(int i = 0; i < shops.Count; i++)
            {
                shopsList.InnerHtml += "<li><a style=\"font-size:16px\" href=\"Shop.aspx?para=" + shops[i]["ID"].ToString() + "\">" + shops[i]["Shop_Name"].ToString() + "</a></li>";
            }
        }
    }
}