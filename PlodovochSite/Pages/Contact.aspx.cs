﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Data.XmlWorker worker = new Data.XmlWorker();

            var xmlData = worker.Read("Contact", "Contacts");

            TableRow row = new TableRow();

            for (int i = 0; i < xmlData.Count; i++)
            {
               

                TableCell cell = new TableCell();

                string content = "<div class=\"block\">";
                content += "<strong><p>"+ xmlData[i]["Position"] + "</p></strong><br/>";
                content += "<strong>ФИО:</strong><a>"+ xmlData[i]["Name"] + "</a><br/>";
                content += "<strong>Телефон:</strong><a>" + xmlData[i]["Phone"] + "</a><br/>";
                content += "<strong>Рабочее время:</strong><a>" + xmlData[i]["WorkTime"] + "</a><br/>";
                content += "<strong>Выходные дни:</strong><a>" + xmlData[i]["DayOff"] + "</a><hr class=\"hr\">";
                content += "</div>";

                cell.Text = content;

                System.Web.HttpBrowserCapabilities browser = Request.Browser;
                if (browser.IsMobileDevice == true) 
                {
                    row = new TableRow();
                    tbl.Rows.Add(row);
                }
                else
                {
                    if (i % 2 == 0)
                    {
                        row = new TableRow();
                        tbl.Rows.Add(row);
                    }
                }
                

                row.Cells.Add(cell);
            }
        }
    }
}