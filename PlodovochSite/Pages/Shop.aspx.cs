﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PlodovochSite.Forms
{
    public partial class Shop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = Request.QueryString["para"];
            Data.OleDbWorker worker = new Data.OleDbWorker();
            var shop = worker.Read("Shops","ID",ID);
            Title = shop[0]["Shop_Name"].ToString();
            Description.InnerText = shop[0]["Shop_Description"].ToString();
            adr.InnerText = shop[0]["Shop_Address"].ToString();
            dirName.InnerText = shop[0]["Shop_DirName"].ToString();
            workTime.InnerText = shop[0]["Shop_WorkTime"].ToString();
            Phone.InnerText = shop[0]["Shop_Phone"].ToString();
            byte[] bytes = (byte[])shop[0]["Shop_Image"];
            shop_Image.Src = "data:Image/png;base64," + Convert.ToBase64String(bytes, 0, bytes.Length);
        }
    }
}