﻿<%@ Page Language="C#" Title="Vacancy" AutoEventWireup="true" CodeBehind="Vacancy.aspx.cs" Inherits="PlodovochSite.Vacancy" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="content">
        <h1 class="labels"><%: Title %></h1>
        <hr/ class="hr">
        <p id ="Description" runat="server" class="labels" style="font-size:16px;text-indent:10px;">Vacancy description</p>
        <h1 class="labels">Мы ожидаем:</h1>
        <hr/ class="hr">
        <ul id="req" runat="server">
        </ul>
        <hr/>
        <p class ="labels"><strong class="labels">Телефон: </strong>45-33-57.</p>
    </div>
</asp:Content>
