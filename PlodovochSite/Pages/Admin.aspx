﻿<%@ Page Title="Админ" Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="PlodovochSite.Forms.Admin" MasterPageFile="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <h1>Admin</h1>
        <hr/ class="hr" />
        <div class="blog">
            <strong><p>Добавить новую запись в блог</p></strong>
            <p><strong>Изображение в формате png, jpg, gif</strong></p>
            <asp:FileUpload ID="imageFile" runat="server" /> 
            <asp:TextBox ID="Blog_Name" runat="server"></asp:TextBox> <asp:Label runat="server" Text="Имя блога"></asp:Label><br/>
            <p>Текст блога</p>
            <asp:TextBox ID="Blog_Description" Height="200px" Width="300px" TextMode="MultiLine" style="overflow:scroll" runat="server"></asp:TextBox><br/>
            <asp:Button ID="upload" runat="server" OnClick="Upload_Blog_Click" Text="Загрузить" /><br/>
            <p>Изменить запись в блоге по ID</p>
            <asp:TextBox ID="editBlogID" runat="server"></asp:TextBox>
        </div>
        <div class="blog">
            <strong><p>Добавить новую вакансию</p></strong>
            <p>Название вакансии</p>
            <asp:TextBox ID="Vacancy_Name" runat="server" />
            <p>Требования</p>
            <asp:TextBox ID="req" runat="server" Height="200px" Width="300px" TextMode="MultiLine" style="overflow:scroll"></asp:TextBox>
            <p>Описание вакансии</p>
            <asp:TextBox ID="Vacancy_Description" Height="200px" Width="300px" TextMode="MultiLine" style="overflow:scroll" runat="server"></asp:TextBox><br/>
            <asp:Button ID="Upload_Vacancy" runat="server" Text="Загрузить" OnClick="Upload_Vacancy_Click" /><br/>
            <p>Изменить вакансию по ID</p>
            <asp:TextBox ID="editVacancyID" runat="server"></asp:TextBox>
        </div>
        <div class="blog">
            <strong><p>Добавить новый магазин</p></strong>
            <p>Название магазина</p>
            <asp:TextBox ID="shop_Name" runat="server" />
            <p>Описание магазина</p>
            <asp:TextBox ID="shop_Description" runat="server" Height="200px" Width="300px" TextMode="MultiLine" style="overflow:scroll"></asp:TextBox>
            <p>Адрес магазина</p>
            <asp:TextBox ID="shop_address" runat="server"></asp:TextBox><br/>
            <p>Контактный номер</p>
            <asp:TextBox ID="shop_phone" runat="server"></asp:TextBox><br/>
            <p>ФИО управляющего</p>
            <asp:TextBox ID="shop_dirName" runat="server"></asp:TextBox><br/>
            <p>Время работы</p>
            <asp:TextBox ID="shop_workTime" runat="server"></asp:TextBox><br/>
            <p>Фото магазина</p>
            <asp:FileUpload ID="shopImage" runat="server"></asp:FileUpload><br/>
            <asp:Button ID="addNewShop" runat="server" Text="Загрузить" OnClick="AddNewShop_Click" /><br/>
            <p>Изменить информацию магазинга по ID</p>
            <asp:TextBox ID="editShopID" runat="server"></asp:TextBox>
            
        </div>
        <div class="blog">
            <strong><p>Удаление</p></strong>
            <p>Удалить запись из блога по ID</p>
            <asp:TextBox ID="Blog_ID" runat="server"></asp:TextBox>
            <asp:Button ID="Delete_Blog" runat="server" Text="Удалить" OnClick="Delete_Blog_Click" /> 
            <hr/>
            <p>Удалить вакансию по ID</p>
            <asp:TextBox ID="Vacancy_ID" runat="server"></asp:TextBox>
            <asp:Button ID="Delete_Vacancy" runat="server" Text="Удалить" OnClick="Delete_Vacancy_Click" />
            <hr />
            <p>Удалить магазин по ID</p>
            <asp:TextBox ID="shop_ID" runat="server"></asp:TextBox>
            <asp:Button ID="Delete_Shop" runat="server" Text="Удалить" OnClick="Delete_Shop_Click" /> 
        </div>
        <div class="blog">
            <strong><p>Обновление информации</p></strong>
            <p>Обновить информацию о предприятии</p>
            <asp:FileUpload ID="aboutUs_File" runat="server"></asp:FileUpload><br/>
            <asp:Button ID="upload_aboutUs" runat="server" Text="Обновить" OnClick="Upload_aboutUs_Click" />
            <hr />
            <p>Обновить список контактных лиц</p>
            <asp:FileUpload ID="contacts_File" runat="server"></asp:FileUpload><br/>
            <asp:Button ID="upload_Contacts" runat="server" Text="Обновить" OnClick="Upload_Contacts_Click" />
            <hr />
            <p>Обновить прайс-лист</p>
            <asp:FileUpload ID="priceList_File" runat="server"></asp:FileUpload><br/>
            <asp:Button ID="upload_PriceList" runat="server" Text="Обновить" OnClick="Upload_PriceList_Click" />
            <hr />
        </div>
</asp:Content>