﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Shop.aspx.cs" Inherits="PlodovochSite.Forms.Shop" MasterPageFile="~/Site.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="content">
        <script src="~/Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
        <h1 class="labels"><%: Title %></h1><hr/ class="hr" />
        <h2 class="labels" id ="Description" runat="server">Shop Description</h2>
        <img src="img" runat="server" id="shop_Image" class="image" />
        <strong class="labels">Адрес: </strong> <a id="adr" class="labels" runat="server"></a><br/>
        <strong class="labels">Время работы: </strong> <a id="workTime" class="labels" runat="server"></a><br/>
        <strong class="labels">ФИО Управляющего: </strong> <a id="dirName" class="labels" runat="server"></a><br/>
        <strong class="labels">Телефоны: </strong><a class="labels" id="Phone" runat="server"></a>
    </div>
</asp:Content>
