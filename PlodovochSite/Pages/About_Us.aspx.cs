﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            Data.XmlWorker worker = new Data.XmlWorker();

            var xmlData = worker.Read("Info", "About");

            for(int i = 0; i < xmlData.Count; i++)
            {
                content.InnerHtml += "<p class=\"labels\" style=\"font-size:40pt\" >" + xmlData[i]["Label"] + "<p><hr/class=\"hr\">";
                content.InnerHtml += "<p class=\"labels\" style=\"font-size:14pt\" >" + xmlData[i]["Text"] + "<p>";
            }
        }
    }
}