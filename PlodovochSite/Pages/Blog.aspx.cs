﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite.Pages
{
    public partial class Blog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = Request.QueryString["para"];
            Data.OleDbWorker worker = new Data.OleDbWorker();
            var data = worker.Read("Blog", "ID", ID);
            Title = data[0]["Blog_Name"].ToString();
            content.InnerHtml += "<p><span class=\"labels\" style=\"font-size:40pt; \">"+Title+"</span></p><hr/ class=\"hr\">";
            byte[] bytes = (byte[])data[0]["Blog_Image"];
            content.InnerHtml += "<img class=\"image\" src=\"" + "data:Image/png;base64," + Convert.ToBase64String(bytes, 0, bytes.Length) + "\">";
            content.InnerHtml += "<p style=\"padding-right:20px; padding-left:20px; color:#7b7979; font-size: 16px; font-family:'Roboto Condensed',sans-serif;\">" + data[0]["Blog_Description"] + "</p>";
        }
    }
}