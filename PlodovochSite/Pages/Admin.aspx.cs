﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace PlodovochSite.Forms
{
    public partial class Admin : System.Web.UI.Page
    {

        //readonly FileUpload file;
        //readonly TextBox Blog_Name;
        //readonly TextBox Blog_Description;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (CheckValidate())
            {

            }
            else
            {
                Response.Redirect("Validate.aspx");
            }
        }


        bool CheckValidate()
        {
            HttpCookie cookie = Request.Cookies["UserData"];
            if (cookie==null)
            {
                return false;
            }
            else
            {
                Data.OleDbWorker worker = new Data.OleDbWorker();
                var users = worker.Read("Users");

                for(int i = 0; i < users.Count; i++)
                {
                    if (cookie["login"] == users[i]["User_Login"].ToString() && cookie["password"] == users[i]["User_Password"].ToString())
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        #region Delete
        protected void Delete_Blog_Click(object sender, EventArgs e)
        {

            try
            {
                string commandQuery = "DELETE FROM Blog WHERE ID =" + Blog_ID.Text;
                Data.OleDbWorker worker = new Data.OleDbWorker();
                worker.Execute(commandQuery);
                Response.Write("<script>alert('Удаление завершено!')</script>");
            }
            catch (Exception exception)
            {
                Response.Write("<script>alert('Удаление не завершено!" + ": " + exception.Message + "')</script>");
            }
        }

        protected void Delete_Vacancy_Click(object sender, EventArgs e)
        {
            try
            {
                string commandQuery = "DELETE FROM Vacancies WHERE ID =" + Vacancy_ID.Text;
                Data.OleDbWorker worker = new Data.OleDbWorker();
                worker.Execute(commandQuery);
                Response.Write("<script>alert('Удаление завершено!')</script>");
            }
            catch (Exception exception)
            {
                Response.Write("<script>alert('Удаление не завершено!" + ": " + exception.Message + "')</script>");
            }
        }

        protected void Delete_Shop_Click(object sender, EventArgs e)
        {
            try
            {
                string commandQuery = "DELETE FROM Shops WHERE ID =" + shop_ID.Text;
                Data.OleDbWorker worker = new Data.OleDbWorker();
                worker.Execute(commandQuery);
                Response.Write("<script>alert('Удаление завершено!')</script>");
            }
            catch (Exception exception)
            {
                Response.Write("<script>alert('Удаление не завершено!" + ": " + exception.Message + "')</script>");
            }
        }
        #endregion
        #region Upload
        protected void Upload_Blog_Click(object sender, EventArgs e)
        {
            if (imageFile.HasFile)
            {
                HttpPostedFile postedFile = imageFile.PostedFile;
                Stream stream = postedFile.InputStream;
                BinaryReader binaryReader = new BinaryReader(stream);
                byte[] bytes = binaryReader.ReadBytes((int)stream.Length);

                string commandQuery="";

                if (editBlogID.Text == "")
                {
                    commandQuery = "INSERT INTO Blog (Blog_Name, Blog_Description, Blog_Image) VALUES (@Blog_Name, @Blog_Description,@Blog_Image)";
                }
                else
                {
                    commandQuery = "UPDATE Blog SET Blog_Name = @Blog_Name, Blog_Description = @Blog_Description, Blog_Image = @Blog_Image WHERE ID =" + editBlogID.Text;
                }
                
                Dictionary<string, object> par = new Dictionary<string, object>();
                par.Add("@Blog_Name", Blog_Name.Text);
                par.Add("@Blog_Description", Blog_Description.Text);
                par.Add("@Blog_Image", bytes);

                Data.OleDbWorker worker = new Data.OleDbWorker();
                worker.Execute(commandQuery, par);

                Response.Write("<script>alert('Новая запись в блоге успешно добавлена!')</script>");
            }
            else
            {
                Response.Write("<script>alert('Новая запись в блоге не добавлена!')</script>");
            }
        }

        protected void Upload_Vacancy_Click(object sender, EventArgs e)
        {
            try
            {
                string commandQuery = "";
                if(editVacancyID.Text == "")
                {
                    commandQuery = "INSERT INTO Vacancies (Vacancy_Name, Vacancy_Description, Vacancy_Requirement) VALUES (@Vacancy_Name, @Vacancy_Description ,@Vacancy_Requirement)";
                }
                else
                {
                    commandQuery = "UPDATE Vacancies SET Vacancy_Name = @Vacancy_Name, Vacancy_Description = @Vacancy_Description, Vacancy_Requirement = @Vacancy_Requirement WHERE ID =" + editVacancyID.Text;
                }

                Data.OleDbWorker worker = new Data.OleDbWorker();
                Dictionary<string, object> par = new Dictionary<string, object>();

                par.Add("@Vacancy_Name", Vacancy_Name.Text);
                par.Add("@Vacancy_Description", Vacancy_Description.Text);
                par.Add("@Vacancy_Requirement", req.Text);

                worker.Execute(commandQuery, par);
                Response.Write("<script>alert('Вакансия успешно добавлена!')</script>");
            }
            catch
            {
                Response.Write("<script>alert('Добавление вакансии не завершено!')</script>");
            }
        }

        protected void Upload_aboutUs_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = aboutUs_File.PostedFile;
            Stream stream = postedFile.InputStream;
            BinaryReader binaryReader = new BinaryReader(stream);
            byte[] bytes = binaryReader.ReadBytes((int)stream.Length);

            if (!File.Exists(Server.MapPath("/Data") + "/About.xml"))
            {
                File.Create(Server.MapPath("/Data") + "/About.xml").Close();
            }
            File.WriteAllBytes(Server.MapPath("/Data") + "/About.xml", bytes);

            Response.Write("<script>alert('Обновление информации завершено!')</script>");
        }

        protected void Upload_Contacts_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = contacts_File.PostedFile;
            Stream stream = postedFile.InputStream;
            BinaryReader binaryReader = new BinaryReader(stream);
            byte[] bytes = binaryReader.ReadBytes((int)stream.Length);

            if (!File.Exists(Server.MapPath("/Data") + "/Contacts.xml"))
            {
                File.Create(Server.MapPath("/Data") + "/Contacts.xml").Close();
            }
            File.WriteAllBytes(Server.MapPath("/Data") + "/Contacts.xml", bytes);

            Response.Write("<script>alert('Обновление информации завершено!')</script>");
        }

        protected void Upload_PriceList_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = priceList_File.PostedFile;
            Stream stream = postedFile.InputStream;

            BinaryReader binaryReader = new BinaryReader(stream);
            byte[] bytes = binaryReader.ReadBytes((int)stream.Length);

            if (!File.Exists(Server.MapPath("/Data") + "/Прайс-лист.xls"))
            {
                File.Create(Server.MapPath("/Data") + "/Прайс-лист.xls").Close();
            }
            File.WriteAllBytes(Server.MapPath("/Data") + "/Прайс-лист.xls", bytes);

            Response.Write("<script>alert('Обновление информации завершено!')</script>");
        }
        #endregion
        #region Add_New
        protected void AddNewShop_Click(object sender, EventArgs e)
        {
            string commandQuery = "";
            if (editShopID.Text == "")
            {
                commandQuery = "INSERT INTO Shops (Shop_Name, Shop_Description, Shop_Address, Shop_DirName, Shop_WorkTime, Shop_Phone, Shop_Image) VALUES (@Shop_Name, @Shop_Description ,@Shop_Address, @Shop_DirName, @Shop_WorkTime, @Shop_Phone, @Shop_Image)";
            }
            else
            {
                commandQuery = "UPDATE Shops SET Shop_Name = @Shop_Name, Shop_Description = @Shop_Description, Shop_Address = @Shop_Address, Shop_DirName = @Shop_DirName, Shop_WorkTime = @Shop_WorkTime, Shop_Phone = @Shop_Phone, Shop_Image = @Shop_Image WHERE ID =" + editShopID.Text;
            }

            if (shopImage.HasFile)
            {
                HttpPostedFile postedFile = shopImage.PostedFile;
                Stream stream = postedFile.InputStream;
                BinaryReader binaryReader = new BinaryReader(stream);
                byte[] bytes = binaryReader.ReadBytes((int)stream.Length);

                Dictionary<string, object> par = new Dictionary<string, object>();
                par.Add("@Shop_Name", shop_Name.Text);
                par.Add("@Shop_Description", shop_Description.Text);
                par.Add("@Shop_Address", shop_address.Text);
                par.Add("@Shop_DirName", shop_dirName.Text);
                par.Add("@Shop_WorkTime", shop_workTime.Text);
                par.Add("@Shop_Phone", shop_phone.Text);
                par.Add("@Shop_Image", bytes);

                Data.OleDbWorker worker = new Data.OleDbWorker();
                worker.Execute(commandQuery, par);

                Response.Write("<script>alert('Магазин успешно добавлен!')</script>");
            }
            else
            {
                Response.Write("<script>alert('Добавление магазина не завершено!')</script>");
            }
        }
        #endregion
    }
}