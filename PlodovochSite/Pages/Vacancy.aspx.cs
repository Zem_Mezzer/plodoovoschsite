﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Text.Json;

namespace PlodovochSite
{
    public partial class Vacancy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = Request.QueryString["para"];
            Data.OleDbWorker worker = new Data.OleDbWorker();
            var vacancy = worker.Read("Vacancies","ID",ID);
            Title = vacancy[0]["Vacancy_Name"].ToString();
            Description.InnerText = vacancy[0]["Vacancy_Description"].ToString();
            Description.InnerHtml += "<hr/>";

            string[] _req = vacancy[0]["Vacancy_Requirement"].ToString().Split('-');

            for(int i = 1; i < _req.Length; i++)
            {
                req.InnerHtml += "<li><p class=\"labels\">" + _req[i] +"</p></li>";
            }
         }
    }
}
