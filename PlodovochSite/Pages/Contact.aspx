﻿<%@ Page Title="Связаться с нами" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="PlodovochSite.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <p><span class="labels" style="font-size:40pt;"><%: Title %>.</span></p>
    <hr/ class="hr">
    <address>
        Торгово-производственное унитарное коммунальное предприятие "Плодоовощ"<br />
        224024, Республика Беларусь, г. Брест, ул. Шоссейная, 2Д,<br />
        УНП 200027599, ОКПО 015680041 <br />
        р/с BY82BLBB30120200027599001001 ОАО «Белинвестбанк»,  МФО BLBBBY2X <br />
    </address>
    <hr/>
    <address> 
        <asp:Table ID="tbl" runat="server" CssClass="tbl content">
        </asp:Table>
    </address>
</asp:Content>
