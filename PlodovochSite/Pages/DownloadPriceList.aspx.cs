﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlodovochSite.Pages
{
    public partial class DownloadPriceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "Excel file/xls";
            Response.AppendHeader("Content-Disposition", "attachment; filename=Прайс-лист.xls");
            Response.TransmitFile(Server.MapPath("~/Data/Прайс-лист.xls"));
            Response.End();
            Response.Redirect("Home.aspx");
        }
    }
}